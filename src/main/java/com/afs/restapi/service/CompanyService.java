package com.afs.restapi.service;

import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;

    public CompanyService(CompanyRepository companyRepository) {
        this.companyRepository = companyRepository;
    }

    public List<Company> getCompanies() {
        return companyRepository.getCompanies();
    }

    public Company getCompanyById(Integer companyId) {
        return companyRepository.getCompanyById(companyId);
    }

    public List<Employee> getEmployeesByCompanyId(Integer companyId) {
        return companyRepository.getEmployeesByCompanyId(companyId);
    }

    public List<Company> getCompaniesByPagination(Integer pageIndex, Integer pageSize) {
        return companyRepository.getCompaniesByPagination(pageIndex, pageSize);
    }

    public void addCompany(Company company) {
        companyRepository.addCompany(company);
    }

    public Company updateCompany(Integer companyId, Company company) {
        return companyRepository.updateCompany(companyId, company);
    }

    public void remove(Integer companyId) {
        companyRepository.remove(companyId);
    }
}
