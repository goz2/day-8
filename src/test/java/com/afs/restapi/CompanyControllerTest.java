package com.afs.restapi;

import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerTest {

    @Autowired
    MockMvc mockMvc;
    ObjectMapper mapper = new ObjectMapper();


    @Autowired
    CompanyRepository companyRepository;

    @BeforeEach
    void setUp() {
        companyRepository.clear();
    }

    @Test
    void should_get_all_companies_when_perform_get_given_companies() throws Exception {
        // given
        generateSpring();

        // when then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("spring"))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].companyName").isString())
                .andExpect(jsonPath("$[1].companyName").value("boot"))
                .andExpect(jsonPath("$[1].employees").isArray());
    }

    @Test
    void should_get_company_by_id_when_perform_get_given_company_id() throws Exception {
        // given
        generateSpring();

        // when then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("spring"))
                .andExpect(jsonPath("$.employees").isArray())
                .andExpect(jsonPath("$.employees.size()").value(3));
    }

    @Test
    void should_get_company_employees_by_id_when_perform_get_given_company_id() throws Exception {
        // given
        generateSpring();

        // when then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/1/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.size()").value(3));
    }

    @Test
    void should_companies_when_perform_get_given_page_and_size() throws Exception {
        // given
        generateSpring();

        // when then
        mockMvc.perform(MockMvcRequestBuilders.get("/companies")
                        .param("pageIndex", "1")
                        .param("pageSize", "1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].companyName").isString())
                .andExpect(jsonPath("$[0].companyName").value("spring"))
                .andExpect(jsonPath("$[0].employees").isArray());
    }

    @Test
    void should_response_data_201_when_perform_post_given_company() throws Exception {
        // given
        generateSpring();
        Company company = new Company();
        company.setCompanyName("oracle");
        String companyJson = mapper.writeValueAsString(company);

        // when then
        mockMvc.perform(MockMvcRequestBuilders.post("/companies").contentType(MediaType.APPLICATION_JSON).content(companyJson))
                .andExpect(status().isCreated());
    }

    @Test
    void should_return_edit_company_info_when_perform_put_given_company_info() throws Exception {
        // given
        generateSpring();
        Company companyInfo = new Company();
        companyInfo.setCompanyName("oracle");
        String companyJson = mapper.writeValueAsString(companyInfo);

        // when then
        mockMvc.perform(MockMvcRequestBuilders.put("/companies/1").contentType(MediaType.APPLICATION_JSON).content(companyJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.companyName").isString())
                .andExpect(jsonPath("$.companyName").value("oracle"));
    }

    @Test
    void should_return_204_when_perform_delete_given_company_id() throws Exception {
        // given

        // when then
        mockMvc.perform(MockMvcRequestBuilders.delete("/companies/1"))
                .andExpect(status().isNoContent());
    }


    @Test
    void should_return_No_FOUND_when_perform_get_by_id_given_companies_in_db_and_id_not_in() throws Exception {
        //given
        generateSpring();

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/companies/6"))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    private void generateSpring() {
        List<Employee> employeesInSpring = new ArrayList<>();
        employeesInSpring.add(new Employee(1, "alice", 21, "female", 6000));
        employeesInSpring.add(new Employee(2, "bob", 20, "male", 6200));
        employeesInSpring.add(new Employee(3, "charles", 22, "mfale", 5800));

        List<Employee> employeesInBoot = new ArrayList<>();
        employeesInBoot.add(new Employee(1, "daisy", 22, "female", 6100));
        employeesInBoot.add(new Employee(2, "ethan", 19, "male", 6000));

        companyRepository.addCompany(new Company(1, "spring", employeesInSpring));
        companyRepository.addCompany(new Company(2, "boot", employeesInBoot));
    }

}
